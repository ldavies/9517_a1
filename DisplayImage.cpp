/*
 * HDR assignment 1 for COMP9517 UNSW
 * Laurence Davies z3342909
 */
#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <dirent.h>
#include <vector>
#include <string>

using namespace cv;
using namespace std;

#define EXIT_COMPLETED 0
#define MAX_WT 443.405006738
#define HDR_LINEAR 0
#define HDR_SQRT 1
#define HDR_LOG 2
#define TOLER 0.0000001


const char * imgwindowname = "HDR Image";
const char * paramwindowname = "Parameters ";


class matHDR {
  public:
    // no constructor.
    string name;
    matHDR(Mat in,string n);
    Mat matRaw; // 8 bits per channel?
    Mat mat; // 8 bits per channel?
    Mat weight; // 32 bits per channel
    float mean[3]; // three channels, this is the mean
    float var[3]; // three channels, this is the variance
    float Brightness;
    int C_low;
    int C_high;
    int c_low_p, c_high_p, brightness_p;
    int rowOffset, colOffset;
    void calc();
    void cropMat(int,int);
};

matHDR::matHDR(Mat in, string n) {
  matRaw = in; // copies reference
  name = n;
  weight = Mat::zeros(matRaw.rows, matRaw.cols, CV_32FC1);
  Brightness = 0;
  C_low = 1;
  C_high = MAX_WT;
  c_low_p = 0;
  c_high_p = 100;
  brightness_p = 0;
  rowOffset = colOffset = 0;
}

void matHDR::cropMat(int rO, int cO) {
  rO += rowOffset;
  cO += colOffset;
  Rect cropTo(cO, rO, matRaw.cols - cO, matRaw.rows - rO);
  mat = matRaw(cropTo);
  cout << "Cropped image " << name << " to " << cropTo << endl;
}

void matHDR::calc() {
  if (!mat.data) {
    mat = matRaw;
    cout << "Mat was NULL. Using matRaw." << endl;
  }
  for ( int i = 0; i < mat.rows; i++)
    for ( int j = 0; j < mat.cols; j++) 
      {
        Vec3b pix = mat.at<Vec3b>(i,j);
        float intensity = 0;
        for ( int k = 0; k < 3; k++ ) intensity += (float)pix.val[k]*(float)pix.val[1];
        intensity = sqrt(intensity);
        float w = (intensity < C_low) ? intensity / (float) C_low : ((intensity > C_high) ? ((MAX_WT - intensity) / (float)(MAX_WT - C_high)) : 1);
        w = (w>TOLER) ? w : TOLER;

        weight.at<float>(i,j) = w;
      }
}


vector<matHDR> images;
Mat HDRImage;
int displayMethod = HDR_LINEAR;

/* Returns number of images
 * Pass string of base directory and pointer to an array of Mat
 */
int getImages(const char * base, vector<matHDR> * images) {

  DIR *dir;
  struct dirent *ent;
  int num = 0;
  string d = string( base );

  if ((dir = opendir (base)) != NULL) {

    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) 
      {

        string file = string( ent->d_name );
        matHDR image = matHDR(imread( ( d + file ), CV_LOAD_IMAGE_COLOR ),file);
      
        if( !image.matRaw.data )
          {
            cout <<  "No image data for " << ent->d_name << endl;
          }
        else
          {
            images->push_back( image );
            num++;
          }
      }

    closedir (dir);
    return num;

  } else {

    /* could not open directory */
    cerr << "Houston, we had a problem." << endl;
    return EXIT_FAILURE;

  }

}


Mat imHist(Mat hist, float scaleX=1, float scaleY=1){
  double maxVal=0;
  minMaxLoc(hist, 0, &maxVal, 0, 0);
  int rows = 64; //default height size
  int cols = hist.rows; //get the width size from the histogram
  Mat histImg = Mat::zeros(rows*scaleX, cols*scaleY, CV_8UC3);
  //for each bin
  for(int i=0;i<cols;i++) {
    float histValue = hist.at<float>(i,0);
    //float nextValue = hist.at<float>(i+1,0);
    Point pt1 = Point(i*scaleX, rows*scaleY);
    Point pt2 = Point(i*scaleX+scaleX, rows*scaleY);
    Point pt3 = Point(i*scaleX+scaleX, (rows-histValue*rows/maxVal)*scaleY);
    Point pt4 = Point(i*scaleX, (rows-histValue*rows/maxVal)*scaleY);
 
    int numPts = 5;
    Point pts[] = {pt1, pt2, pt3, pt4, pt1};
 
    fillConvexPoly(histImg, pts, numPts, Scalar(255,255,255));
  }
  return histImg;
}
 
 
void showHistogram(Mat img, string histname, int rangemax) {
 
  //Hold the histogram
  MatND hist;
  Mat histImg;
  int nbins = 256; // lets hold 256 levels
  int hsize[] = { nbins }; // just one dimension
  float range[] = { 0, rangemax };
  const float *ranges[] = { range };
  int chnls[] = {0};
 
  // create colors channels
  vector<Mat> colors;
  split(img, colors);
 
  // compute for all colors
  calcHist(&colors[0], 1, chnls, Mat(), hist,1,hsize,ranges);
  histImg = imHist(hist);
  imshow((histname + " Blue").c_str(),histImg);
 
  calcHist(&colors[1], 1, chnls, Mat(), hist,1,hsize,ranges);
  histImg = imHist(hist);
  imshow((histname + " Green").c_str(),histImg);
 
  calcHist(&colors[2], 1, chnls, Mat(), hist,1,hsize,ranges);
  histImg = imHist(hist);
  imshow((histname + " Red").c_str(),histImg);
 
}


Point matchImages(const Mat& img, const Mat& _b)
{
    //const Mat *img = &a, *_b = &b;
    Rect bounds(_b.cols/4,_b.rows/4,3*_b.cols/4,3*_b.rows/4);
    Mat templ = _b(bounds); 

    /// Create the result matrix
    int result_cols =  img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;

    Mat result( result_cols, result_rows, CV_32FC1 );

    /// Do the Matching and Normalize
    matchTemplate(img, templ, result, CV_TM_CCOEFF);
    normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

    Point maxLoc;
    minMaxLoc(result, NULL, NULL, NULL, &maxLoc);

    return Point(maxLoc.x - _b.cols/4, maxLoc.y - _b.rows/4);
}

void recalcHDR() {
  int rows = 32767;
  int cols = 32767;
  for (int p = 0; p < images.size(); p++)
    {
      images[p].calc();
      rows = min(images[p].mat.rows,rows);
      cols = min(images[p].mat.cols,cols);
    }

  HDRImage = Mat::zeros(rows, cols, CV_32FC3);
//  HDRImageQ = Mat::zeros(rows, cols, CV_8UC3);
  for ( int i = 0; i < rows; i++)
    for ( int j = 0; j < cols; j++) 
      {
        float wBGR = 0;
        float B = 0;
        float G = 0;
        float R = 0;
        for (int k = 0; k < images.size(); k++)
          {
            Vec3b p = images[k].mat.at<Vec3b>(i,j);
            float w = images[k].weight.at<float>(i,j);
            wBGR += w;
            B += w * (p.val[0] + images[k].Brightness);
            G += w * (p.val[1] + images[k].Brightness);
            R += w * (p.val[2] + images[k].Brightness);
          }
        
        HDRImage.at<Vec3f>(i,j) = Vec3f(B/wBGR, G/wBGR, R/wBGR);
        //HDRImageQ.at<Vec3b>(i,j) = Vec3b(B/wB, G/wG, R/wR);
      }

  // Normalise resultant image
  Mat Dest;


  switch (displayMethod)
    {
      case HDR_LOG:
        cout << "Log() scaling the resulting image" << endl;
        log(HDRImage + 1, Dest);
        max(Dest, 0.0, Dest);
        min(Dest, 10, Dest);
        break;
      case HDR_SQRT:
        cout << "SQRT() scaling the resulting image" << endl;
        sqrt(HDRImage, Dest);
        break;
      case HDR_LINEAR: default: // do nothing
        cout << "Linear scaling the resulting image" << endl;
        Dest = HDRImage;
    }

  //normalize(HDRImage, Dest, 0.0, 255, (int)NORM_MINMAX, -1); 
  try
    {
      double maxval = 0;
      double minval = 0;
    
      minMaxLoc(Dest, &minval, &maxval);

      Dest -= minval;
      Dest /= maxval - minval;
    
      cout << "Min: " << minval << " Max: " << maxval << endl;
      minMaxLoc(images[0].weight, &minval, &maxval);
      cout << "Weight Min: " << minval << " Max: " << maxval << endl;
    }
  catch (Exception e)
    {
      cout << "Failed to minmaxloc the normalized matrix" << endl;
    }

  
  showHistogram(Dest,string("Output histogram"), 1);

  imshow( imgwindowname , Dest );
}
  

// callback for slider changes
void c_low_callback( int position , void * im ){
  ((matHDR *)im)->C_low = MAX_WT * (float)position / 100;
}
void c_high_callback( int position , void * im ){
  ((matHDR *)im)->C_high = MAX_WT * (float)position / 100;
}
void brightness_callback( int position , void * im ){
  ((matHDR *)im)->Brightness = (float)position * 2.55;
}
void mouseEvent(int evt, int x, int y, int flags, void* param){
  if(evt==CV_EVENT_LBUTTONUP){
    recalcHDR();
  }
}

int main( int argc, char** argv )
{
  if ( argc < 2 ) 
    {
      cout << "Please specify an image for the first argument." << endl;
      return 1;
    }

  int numImages = getImages( argv[1] , &images );
  bool skipTemplateMatch = (argc >= 3 && atoi(argv[2]) > 0) ? true : false;
  if (argc >= 4)
    {
      switch ( atoi(argv[3]))
        {
          case 1: displayMethod = HDR_SQRT; break;
          case 2: displayMethod = HDR_LOG; break;
          case 0: default : displayMethod = HDR_LINEAR; break;
        }
    }

  if( numImages == 0 || numImages == EXIT_FAILURE )
    {
      cout <<  "No images were found." << endl;
      return EXIT_FAILURE;
    }

  namedWindow( imgwindowname , CV_WINDOW_AUTOSIZE );
  
  int i = 0;
  int curPWindow = 0;
  while (i < numImages)
    {
      char ia[5];
      sprintf(ia,"%d",i);

      char pa[5];
      sprintf(pa,"%d",curPWindow);


      string pname = (string(paramwindowname) + pa); 
      namedWindow( pname.c_str() , CV_WINDOW_AUTOSIZE );

      createTrackbar((images[i].name + " C_low " + ia).c_str() , pname.c_str(), &images[i].c_low_p, 100, c_low_callback, (void *)(&(images[i])) );
      createTrackbar((images[i].name + " C_high " + ia).c_str(), pname.c_str(), &images[i].c_high_p, 100, c_high_callback, (void *)(&(images[i])) );
      createTrackbar((images[i].name + " Offset " + ia).c_str()  , pname.c_str(), &images[i].brightness_p, 100, brightness_callback, (void *)(&(images[i])) );

      i++;
      if (i % 4 == 0)
        {
          curPWindow++;
        }
    }
  setMouseCallback(imgwindowname,mouseEvent,0);

  if (!skipTemplateMatch)
    {
      int rO = 0;
      int cO = 0;
      for ( i = 0; i < numImages; i++ )
        {
    
          Point m = matchImages(images[0].matRaw,images[i].matRaw);
    
          images[i].rowOffset = -m.y;
          images[i].colOffset = -m.x;
    
          rO = max(rO, m.y);
          cO = max(cO, m.x);
    
          cout << "Image " << i << " match at point " << m << endl;
    /*
          char ia[5];
          sprintf(ia,"%d",i);
          string hname = (string("Hist ") + ia); 
    
          showHistogram((images[i].mat),hname);
    */
        }
    
      for ( i = 0; i < numImages; i++ )
        {
          images[i].cropMat(rO, cO);
        }
    }

  recalcHDR();

  waitKey(0);

  return EXIT_COMPLETED;
}
