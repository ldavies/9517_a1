#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <dirent.h>
#include <vector>
#include <string>

using namespace cv;
using namespace std;

#define EXIT_COMPLETED 0


const char * imgwindowname = "HDR Image";
const char * paramwindowname = "Parameters";


class matHDR {
  public:
    // no constructor.
    matHDR(Mat in);
    Mat mat; // 8 bits per channel?
    Mat weight; // 32 bits per channel
    float mean[3]; // three channels, this is the mean
    float var[3]; // three channels, this is the variance
    float Brightness;
    int C_low;
    int C_high;
    int c_low_p, c_high_p, brightness_p;
    int xOffset, yOffset;
    void calc();
};

matHDR::matHDR(Mat in) {
  mat = in; // copies reference
  weight = Mat::zeros(mat.rows, mat.cols, CV_32FC3);
  Brightness = 0;
  C_low = 1;
  C_high = 1;
  c_low_p = 0;
  c_high_p = 255;
  brightness_p = 0;
  xOffset = yOffset = 0;
}

void matHDR::calc() {
  //weight = Mat::zeros(mat.rows, mat.cols, CV_32FC3);
  for ( int i = 0; i < mat.rows; i++)
    for ( int j = 0; j < mat.cols; j++) 
      {
        Vec3b pix = mat.at<Vec3b>(i,j);
        Vec3f w = Vec3f(0,0,0);
        for ( int k = 0; k < 3; k++ )
          {
            w.val[k] = (pix.val[k] < C_low) ? ((float)(pix.val[k]) / (float) C_low) : ((pix.val[k] > C_high) ? ((float)(255 - pix.val[k]) / (float)(255 - C_high)) : 1);
          }

        weight.at<Vec3f>(i,j) = w;
      }
}


vector<matHDR> images;
Mat HDRImage;
Mat HDRImageQ;

/* Returns number of images
 * Pass string of base directory and pointer to an array of Mat
 */
int getImages(const char * base, vector<matHDR> * images) {

  DIR *dir;
  struct dirent *ent;
  int num = 0;
  string d = string( base );

  if ((dir = opendir (base)) != NULL) {

    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) 
      {

        string file = string( ent->d_name );
        matHDR image = matHDR(imread( ( d + file ), CV_LOAD_IMAGE_COLOR ));
      
        if( !image.mat.data )
          {
            cout <<  "No image data for " << ent->d_name << endl;
          }
        else
          {
            images->push_back( image );
            num++;
          }
      }

    closedir (dir);
    return num;

  } else {

    /* could not open directory */
    cerr << "Houston, we had a problem." << endl;
    return EXIT_FAILURE;

  }

}


void showHistogram(Mat * hsv, const char * histname) {
  // Quantize the hue to 30 levels
  // and the saturation to 32 levels
  int hbins = 30, sbins = 32;
  int histSize[] = {hbins, sbins};
  // hue varies from 0 to 179, see cvtColor
  float hranges[] = { 0, 180 };
  // saturation varies from 0 (black-gray-white) to
  // 255 (pure spectrum color)
  float sranges[] = { 0, 256 };
  const float* ranges[] = { hranges, sranges };
  MatND hist;
  // we compute the histogram from the 0-th and 1-st channels
  int channels[] = {0, 1};

  calcHist( hsv, 1, channels, Mat(), // do not use mask
            hist, 2, histSize, ranges,
            true, // the histogram is uniform
            false );
  double maxVal=0;
  minMaxLoc(hist, 0, &maxVal, 0, 0);

  int scale = 10;
  Mat histImg = Mat::zeros(sbins*scale, hbins*10, CV_8UC3);

  for( int h = 0; h < hbins; h++ )
    for( int s = 0; s < sbins; s++ )
      {
        float binVal = hist.at<float>(h, s);
        int intensity = cvRound(binVal*255/maxVal);
        rectangle( histImg, Point(h*scale, s*scale),
                   Point( (h+1)*scale - 1, (s+1)*scale - 1),
                   Scalar::all(intensity),
                   CV_FILLED );
      }

  namedWindow( histname, 1 );
  imshow( histname, histImg );
}

void recalcHDR() {
  int rows = 32767;
  int cols = 32767;
  for (int p = 0; p < images.size(); p++)
    {
      images[p].calc();
      rows = min(images[p].mat.rows,rows);
      cols = min(images[p].mat.cols,cols);
    }

  HDRImage = Mat::zeros(rows, cols, CV_32FC3);
  HDRImageQ = Mat::zeros(rows, cols, CV_8UC3);
  for ( int i = 0; i < rows; i++)
    for ( int j = 0; j < cols; j++) 
      {
        float wB = 0;
        float wG = 0;
        float wR = 0;
        float B = 0;
        float G = 0;
        float R = 0;
        for (int k = 0; k < images.size(); k++)
          {
            Vec3b p = images[k].mat.at<Vec3b>(i,j);
            Vec3b w = images[k].weight.at<Vec3f>(i,j);
            wB += w.val[0];
            wG += w.val[1];
            wR += w.val[2];
            B += w.val[0] * (p.val[0] + images[k].Brightness);
            G += w.val[1] * (p.val[1] + images[k].Brightness);
            R += w.val[2] * (p.val[2] + images[k].Brightness);
          }
        
        HDRImage.at<Vec3f>(i,j) = Vec3f(B/wB, G/wG, R/wR);
        //HDRImageQ.at<Vec3b>(i,j) = Vec3b(B/wB, G/wG, R/wR);
      }

  // Normalise resultant image
  //Mat Dest;
  //normalize(HDRImage, Dest, 0.0, 255, (int)NORM_MINMAX, -1); 

  //HDRImageQ =  Dest * 255;

  try
    {
      double maxval = 0;
      double minval = 0;
    
      minMaxLoc(HDRImage, &minval, &maxval);

      HDRImage -= minval;
      HDRImage /= maxval - minval;
    
      cout << "Min: " << minval << " Max: " << maxval << endl;
      minMaxLoc(images[0].weight, &minval, &maxval);
      cout << "Weight Min: " << minval << " Max: " << maxval << endl;
    }
  catch (Exception e)
    {
      cout << "Failed to minmaxloc the normalized matrix" << endl;
    }

  

  imshow( imgwindowname , HDRImage );
}
  

// callback for slider changes
void c_low_callback( int position , void * im ){
  ((matHDR *)im)->C_low = position + 1;
}
void c_high_callback( int position , void * im ){
  ((matHDR *)im)->C_high = position + 1;
}
void brightness_callback( int position , void * im ){
  ((matHDR *)im)->Brightness = (float)position/255.0;
}
void mouseEvent(int evt, int x, int y, int flags, void* param){
  if(evt==CV_EVENT_LBUTTONUP){
    recalcHDR();
  }
}

int main( int argc, char** argv )
{
  if ( argc < 2 ) 
    {
      cout << "Please specify an image for the first argument." << endl;
      return 1;
    }

  int numImages = getImages( argv[1] , &images );

  if( numImages == 0 )
    {
      cout <<  "No images were found." << endl;
      return -1;
    }

  namedWindow( imgwindowname , CV_WINDOW_AUTOSIZE );
  namedWindow( paramwindowname , CV_WINDOW_AUTOSIZE );


  for ( int i = 0; i < numImages; i++ )
    {
      char ia[5];
      sprintf(ia,"%d",i);
      createTrackbar((string("C_low") + ia).c_str() , paramwindowname, &images[i].c_low_p, 253, c_low_callback, (void *)(&(images[i])) );
      createTrackbar((string("C_high") + ia).c_str(), paramwindowname, &images[i].c_high_p, 253, c_high_callback, (void *)(&(images[i])) );
      createTrackbar((string("Brightness") + ia).c_str()  , paramwindowname, &images[i].brightness_p, 253, brightness_callback, (void *)(&(images[i])) );
    }

  setMouseCallback(imgwindowname,mouseEvent,0);

  recalcHDR();

  waitKey(0);

  return EXIT_COMPLETED;
}
